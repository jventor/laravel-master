@extends ('layouts.template')
@section ('styles')
	@parent
    {{-- Aplicando estilos personalizados --}}
@stop

@section ('content')

<h1>Lista de usuarios</h1>
<?php $status=Session::get('status'); ?>
@if ($status=='ok_create')
<div class="alert alert-success fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <strong>Aviso:</strong> El usuario se ha creado con exito
</div>
@endif
@if ($status=='ok_delete')
<div class="alert alert-danger fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <strong>Aviso:</strong> El usuario se ha eliminado con exito
</div>
@endif
@if ($status=='ok_update')
<div class="alert alert-warning fade in" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <strong>Aviso:</strong> El usuario se ha editado con exito
</div>
@endif
<table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>id</th>
                <th>username</th>
                <th>email</th>
                <th>level</th>
                <th>creado</th>
                <th>modificado</th>
                <th>operaciones</th>
            </tr>
        </thead>
 
        <tbody>
@if($users)
	@foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->level}}</td>
                <td>{{ $user->created_at }}</td>
                <td>{{ $user->updated_at }}</td>
                <td>
                	<span class="label label-info"><a href="{{ url('users/edit') }}/{{ $user->id }}">Editar</a> </span>
                	<span class="label label-success">Enviar mensaje</span>
                	<span class="label label-danger">{{ HTML::link('#','Borrar', array('class'=>'delete','title'=>$user->id)) }}</span></td>
            </tr>
	@endforeach
        </tbody>
     </table>
@endif
<a href="{{ url('users/new') }}" class="btn btn-primary btn-md" role="button">Nuevo usuario</a>
@stop
@section('scripts')
<script type="text/javascript">
	$(document).ready(function() {
	    $('#example').dataTable();
	} );
</script>
<script>
	$(document).ready(function() {
		$('.delete').click (function(){
			if (confirm("¿Está usted seguro de que desea eliminar?")){
				var id = $(this).attr ("title");
				document.location.href='users/delete/'+id;
			}
		})
	} );
</script>

@stop
