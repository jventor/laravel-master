@extends ('layouts.template')
@section ('styles')
	@parent
    {{-- Aplicando estilos personalizados --}}
		{{ HTML::style('assets/css/login.css') }}
@stop

@section ('content')
<br>
  @if ($action == 'new')   
    <form action="{{ url('users/create') }}" method="POST" class="form-user">
    	<h2 class="form-user-heading">Nuevo usuario</h2>
  @else
      <form action="{{ url('users/update') }}" method="POST" class="form-user">
      <h2 class="form-user-heading">Editar usuario {{ $user->id }}</h2>
      <input type="hidden" name="user_id" value="{{ $user->id }}">
  @endif
		{{ Form::token() }}
        @if(isset($errors))
        <p style="color:#FB1D1D">
           <ul>
              @foreach($errors as $item)
                 <li style="color:#FB1D1D"> {{ $item }} </li>
              @endforeach
           </ul>
        </p>
        @endif

		<div >
        	<input type="text" placeholder="Username" name="username" @if(isset($user)) value="{{ $user->username }}" @endif>
    	</div>
      @if ($action == 'new')  
    	<div>
	        <input type="password" placeholder="Password" name="password">
    	</div>
      @endif
	    <div>
	        <input type="text" placeholder="E-mail" name="email" @if(isset($user)) value="{{ $user->email }}" @endif>
    	</div>
	    <div>
	        <input type="text" placeholder="Level" name="level" @if(isset($user)) value="{{ $user->level }}" @endif>
    	</div>
        <button class="btn btn-large btn-primary" type="submit">Guardar</button>
    </form>
<br>	
@stop