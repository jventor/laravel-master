@extends ('layouts.template')
@section ('styles')
	@parent
    {{-- Aplicando estilos personalizados --}}

@stop

@section ('content')
<br> <br> <br> <br> 
<h2>Has sido deslogueado correctamente.</h2>
<br> <br> <br> <br> 
@stop

@section('scripts')
<script type="text/javascript">
	function redireccionar(){
	    window.location="{{ url('/') }}";
	} 
	setTimeout ("redireccionar()", 2000);
</script>
@stop