@extends ('layouts.template')
@section ('styles')
	@parent
    {{-- Aplicando estilos personalizados --}}
{{ HTML::style('assets/css/login.css') }}
@stop

@section ('content')

      <form action="{{ url('login') }}" method="POST" class="form-signin">
      	<h2 class="form-signin-heading">Formulario de acceso</h2>
      	{{ Form::token() }}
		@if (Session::has('login_errors'))
		<p style='color:#FB1D1D'>El nombre de usuario o la contraseña no son correctos.</p>
		@else
		<p>Introduzca usuario y contraseña para continuar.</p>
		@endif

        <input type="text" class="input-block-level" placeholder="Username" name="username">
        <input type="password" class="input-block-level" placeholder="Password" name="password">
        <label class="checkbox">
          <input type="checkbox" value="remember-me"> Recuerdame
        </label>
        <button class="btn btn-large btn-primary" type="submit">Sign in</button>
      </form>
<br> <br> <br> <br> 
@stop
