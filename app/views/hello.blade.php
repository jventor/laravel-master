@extends ('layouts.template')



@section ('content')
{{ trans('messages.welcome')}}
<br>
{{ Session::get('lang', 'es') }}
	@parent

	{{-- @include('layouts.includes.carousel') --}}
	{{-- <div id="summernote">Hello Summernote</div> --}}
@stop

@section ('scripts')
<script>
$(document).ready(function() {
  $('#summernote').summernote({
  	toolbar: [
    //[groupname, [button list]]
     
    ['style', ['bold', 'italic', 'underline', 'clear']],
    ['font', ['strikethrough']],
    ['fontsize', ['fontsize']],
    ['color', ['color']],
    ['para', ['ul', 'ol', 'paragraph']],
    ['height', ['height']],
  ]
  });
});
</script>
@endsection