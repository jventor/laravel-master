<nav class="navbar navbar-inverse" role="navigation" style="margin:0;"
  <div class="container-fluid">
    {{-- Brand and toggle get grouped for better mobile display --}}
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>

      <a class="navbar-brand visible-xs" href="#">@yield('appname')</a>
    </div>

    {{-- Collect the nav links, forms, and other content for toggling --}}
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      {{-- Inicio Elementos Menú --}}
        {{-- Elemento Menú: Link --}}
        <li class="active"><a href="{{ url('/') }}">{{ trans('messages.home')}}</a></li>
        {{-- Elemento Menú: Dropdown --}}
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Link2</a></li>
            <li class="divider"></li>
            <li><a href="#">Link 3</a></li>
          </ul>
        </li>
      {{-- Fin Elementos Menú --}}
      </ul>
      {{-- Elementos derecha de menú: disponibles nav-flags = banderas y nav-log = botones de login y logout --}}
      @yield('right-nav')
      {{-- Fin Elementos Cambio de idioma (opcional) --}}
    </div>
  </div>
</nav>