<div id="carousel-slider" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-slider" data-slide-to="1"></li>
  </ol>

  <!-- Wrapper for slides -->

  <div class="carousel-inner">
        <div class="active item"><img  src="{{ asset('assets/images/slide/image1.jpg') }}" alt="banner1" /></div>
        <div class="item"><img  src="{{ asset('assets/images/slide/image2.jpg') }}" alt="banner2" /></div>
  </div>

  <!-- Controls -->
  <a class="left carousel-control" href="#carousel-slider" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
  </a>
  <a class="right carousel-control" href="#carousel-slider" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
  </a>
</div>