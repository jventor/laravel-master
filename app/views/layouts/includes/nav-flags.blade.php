      <ul class="nav navbar-nav navbar-right">
      	@if (Session::get('lang') == 'en')
 	      <li><a href="{{ url('/changelang/es') }}"><img  src="{{ asset('assets/images/flags/es.png') }}" alt="Español" /></a></li>
 	    @else  
          <li><a href="{{ url('/changelang/en') }}"><img  src="{{ asset('assets/images/flags/gb.png') }}" alt="English" /></a></li>
        @endif
      </ul>

