      <ul class="nav navbar-nav navbar-right">
      @if (Auth::check())
			   <li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-lg" alt="logout"> {{ trans('messages.user')}}: {{ Auth::user()->username }}</i></a></li>
		  @else
			   <li><a href="{{ url('login') }}"><i class="fa fa-sign-in fa-lg" alt="login"></i></a></li>
		  @endif
      </ul>


