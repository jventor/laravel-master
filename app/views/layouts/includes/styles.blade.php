    {{-- Bootstrap --}}
    {{ HTML::style('assets/css/bootstrap.min.css', array('media' => 'screen')) }}
    {{-- Font Awesome --}}
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    {{-- DataTables CSS --}}
	<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.1/css/jquery.dataTables.css">
    {{-- Aplicando estilos personalizados --}}
    {{ HTML::style('assets/css/styles.css') }}
    
    {{-- Summernote editor wysiwyg --}}
    {{ HTML::style('assets/css/summernote.css') }}

