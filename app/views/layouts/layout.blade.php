<!DOCTYPE html>
<html>
  <head>
    <title>@yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @section('styles')
    @include('layouts.includes.styles')
    @show
    {{-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries --}}
    <!--[if lt IE 9]>
        {{ HTML::script('assets/js/html5shiv.js') }}
        {{ HTML::script('assets/js/respond.min.js') }}
    <![endif]-->
  </head>
  <body>
    {{-- Wrap all page content here --}}
    <div id="wrap">
      {{-- Begin page content --}}
        @include('layouts.header')
        <div class="container">
          @section('content')
          <h1>Content</h1>
          @show
        </div>
        @include('layouts.footer')
    </div>
    
    {{-- jQuery (necessary for Bootstrap's JavaScript plugins) --}}
    <script src="//code.jquery.com/jquery.js"></script>
    
    {{-- Include all compiled plugins (below), or include individual files as needed --}}
    {{ HTML::script('assets/js/bootstrap.min.js') }}

    {{-- Include all compiled plugins (below), or include individual files as needed --}}
    {{ HTML::script('assets/js/summernote.min.js') }}

    {{-- DataTables --}}
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.1/js/jquery.dataTables.js"></script>
    {{-- scripts jquery de la pagina --}}
    @yield('scripts')

  </body>
</html>