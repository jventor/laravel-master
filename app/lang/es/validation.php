<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines contain the default error messages used by
	| the validator class. Some of these rules have multiple versions such
	| as the size rules. Feel free to tweak each of these messages here.
	|
	*/

	"accepted"             => ":attribute debe ser aceptado.",
	"active_url"           => ":attribute no es una url valida.",
	"after"                => ":attribute debe ser una fecha posterior a :date.",
	"alpha"                => ":attribute solo puede contener letras.",
	"alpha_dash"           => ":attribute solo puede contener letras, números, y dashes.",
	"alpha_num"            => ":attribute solo puede contener letras y números",
	"array"                => ":attribute debe ser un array.",
	"before"               => ":attribute debe ser una fecha anterior a :date.",
	"between"              => array(
		"numeric" => ":attribute debe estar entre :min y :max.",
		"file"    => ":attribute debe tener entre :min y :max kilobytes.",
		"string"  => ":attribute debe tener entre :min y :max caracteres.",
		"array"   => ":attribute debe tener entre :min y :max elementos.",
	),
	"boolean"              => ":attribute debe ser true o false",
	"confirmed"            => ":attribute confirmación no coincide.",
	"date"                 => ":attribute no es una fecha válida.",
	"date_format"          => ":attribute no coincide con el formato :format.",
	"different"            => ":attribute y :other deben ser diferentes.",
	"digits"               => ":attribute deben ser :digits digitos.",
	"digits_between"       => ":attribute debe tener entre :min and :max digitos.",
	"email"                => ":attribute debe ser email válido.",
	"exists"               => ":attribute seleccionado es inválido.",
	"image"                => ":attribute debe ser una imagen.",
	"in"                   => ":attribute seleccionado es inválido.",
	"integer"              => ":attribute debe ser un entero.",
	"ip"                   => ":attribute debe ser una dirección IP válida.",
	"max"                  => array(
		"numeric" => ":attribute no puede ser mayor de :max.",
		"file"    => ":attribute no puede ser mayor de :max kilobytes.",
		"string"  => ":attribute no puede ser mayor de :max caracteres.",
		"array"   => ":attribute may not have more than :max elementos.",
	),
	"mimes"                => ":attribute debe ser un fichero de tipo: :values.",
	"min"                  => array(
		"numeric" => ":attribute debe de ser de al menos :min.",
		"file"    => ":attribute debe de ser de al menos :min kilobytes.",
		"string"  => ":attribute debe de ser de al menos :min caracteres.",
		"array"   => ":attribute debe tener al menos :min elementos.",
	),
	"not_in"               => ":attribute seleccionado es inválido.",
	"numeric"              => ":attribute debe ser numérico.",
	"regex"                => "El formado de :attribute es inválido.",
	"required"             => "Campo :attribute es requerido.",
	"required_if"          => "Campo :attribute es requerido cuando :other es :value.",
	"required_with"        => "Campo :attribute es requerido cuando :values está presente.",
	"required_with_all"    => "Campo :attribute es requerido cuando :values está presente.",
	"required_without"     => "Campo :attribute es requerido cuando :values están presente.",
	"required_without_all" => "Campo :attribute es requerido cuando ninguno de :values están presentes.",
	"same"                 => ":attribute y :other deben coincidir.",
	"size"                 => array(
		"numeric" => ":attribute debe ser de :size.",
		"file"    => ":attribute debe ser de :size kilobytes.",
		"string"  => ":attribute debe ser de :size caracteres.",
		"array"   => ":attribute debe contener :size elementos.",
	),
	"unique"               => ":attribute ya ha sido usado.",
	"url"                  => "El formato  de :attribute es inválido.",

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Language Lines
	|--------------------------------------------------------------------------
	|
	| Here you may specify custom validation messages for attributes using the
	| convention "attribute.rule" to name the lines. This makes it quick to
	| specify a specific custom language line for a given attribute rule.
	|
	*/

	'custom' => array(
		'attribute-name' => array(
			'rule-name' => 'custom-message',
		),
	),

	/*
	|--------------------------------------------------------------------------
	| Custom Validation Attributes
	|--------------------------------------------------------------------------
	|
	| The following language lines are used to swap attribute place-holders
	| with something more reader friendly such as E-Mail Address instead
	| of "email". This simply helps us make messages a little cleaner.
	|
	*/

	'attributes' => array(),

);
