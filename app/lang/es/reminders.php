<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Las contraseñas deben ser de al menos 6 caracteres y coincidir con la confirmación.",

	"user" => "No podemos encontrar un usuario con esa dirección de e-mail.",

	"token" => "Este password reset token es inválido.",

	"sent" => "¡Recordario de contraseña enviado!!",

);
