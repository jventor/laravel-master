<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Ruta inicial */
Route::get('/', function(){
	return View::make('hello');
});

/* Ruta zona administración */
Route::get('admin', array('before' => 'auth', function()
{
    return View::make('adminPage');
}));
/*
Route::get('changelang/{lang?}', function($lang = 'es'){
	{{ App::setLocale($lang) }}
	return Redirect::to('/');
});
*/
Route::get('changelang/{lang?}', 'LanguageController@select');
/*
Route::get('language/{lang}', 
           array(
                  'as' => 'language.select', 
                  'uses' => 'LanguageController@select'
                 )
          );
*/
Route::get('login', 'UserLogin@open');

Route::post('login', 'UserLogin@user');

Route::get('logout', 'UserLogin@close');

/* RUtas de sistema */
Route::controller('users','UsersController');
Route::controller('user/getuser','getuserController');
