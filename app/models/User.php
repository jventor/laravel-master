<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

	public function getRememberToken(){
    	return $this->remember_token;
	}
	
	public function setRememberToken($value){
	    $this->remember_token = $value;
	}

	public function getRememberTokenName(){
	    return 'remember_token';
	}

	//*************
	//* Validación de formulario
	//**************
	//Personalizadmos los mensajes de error
   public static $messages = array(
      'username.required' => 'El nombre de usuario es obligatorio.',
      'username.min' => 'El nombre debe contener al menos dos caracteres.',
      'username.max' => 'El nombre puede contener máximo 20 caracteres.',
      'email.required' => 'El email es obligatorio.',
      'email.email' => 'El email debe contener un formato válido.',
      'email.unique' => 'El email pertenece a otro usuario.',
      'password.required' => 'La contraseña es obligatoria.',
      'password.min' => 'La contraseña debe contener al menos seis caracteres.',
      'level.required' => 'El level es obligatorio.(0 usuario, 1 administrador',
      'level.numeric' => 'El nivel debe ser numérico.'
   );
   // Creamos las reglas
	public static $rules = array(
      'username' => 'required|min:2|max:20',
      'email' => 'required|email|unique:users,email,id',
      'password' => 'required|min:6',
      'level' => 'required|numeric'
   );
	// Creamos la función que va a validar
   public static function validate($data, $id=null){
      $reglas = self::$rules;
      $reglas['email'] = str_replace('id', $id, self::$rules['email']);
      $messages = self::$messages;
      return Validator::make($data, $reglas,$messages);
   }
}
