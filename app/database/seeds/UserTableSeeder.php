<?php

class UserTableSeeder extends Seeder {
	public function run(){
	DB::table('users')->delete();

	User::create(array(
		'username' => 'root',
		'email' => 'root@detenerife.es',
		'password' => Hash::make('root')
	));

	User::create(array(
		'username' => 'user',
		'email' => 'user@detenerife.es',
		'password' => Hash::make('user')
	));
}	
}