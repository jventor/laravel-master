<?php
class getuserController extends BaseController{
	public function postData(){
		$user_id = Input::get('user');
		$user = User::find($user_id);

		$data = array(
			'success' => true,
			'id' => $user->id,
			'username' => $user->username,
			'email' => $user->email,
			'level' => $user->level
			);
		return Response::json($data);
	}
}