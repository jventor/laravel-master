<?php
class UsersController extends BaseController{
	/*
	** Función: Constructor
	** Descripción: 
	** Parametrós: --
	** Retorno: --
	*/

	public function __construct(){
		$this->beforefilter('auth'); // Se ejecuta el filtro de autentificación antes de crearse
	}

	/*
	** Función: getIndex
	** Descripción: devuelve la vista inicial
	** Parametrós: --
	** Retorno: --
	*/
	public function getIndex(){
		$my_id = Auth::user()->id; // Cargamos el id del usuario actual
		$level = Auth::user()->level; // Cargamos el level de acceso del usuario actual

		// Si es administardor se muestran todos los usuarios (omitiendole a él mismo y a los administradores)
		if ($level == 1){ 
			$users = DB::table('users')->where('level','<>','1')->where('id','<>',$my_id)->get();
			return View::make('users.index')->with('users',$users);
		}
		else{
		// En caso de que no sea administrador no  tiene acceso a la lista de usuarios
			return View::make('errors.access_denied_ad');
		}
	}

	/*
	** Función: getNew
	** Descripciçon: Responde a la petición get de new
	** Parametrós:
	** Retorno: Retorna la vista de alta de usuario (action = new)
	*/
	public function getNew(){
			return View::make('users.formuser')->with('action','new');
	}

	/*
	** Función:
	** Descripciçon:
	** Parametrós:
	** Retorno
	*/
	public function postCreate(){
		// Creamos el nuevo objeto usuario
		$user = new User;
		// Cargamos los datos del formulario en el objeto usuario
		$user->username = Input::get('username');
		$user->email = Input::get('email');
		$user->level = Input::get('level');
		$user->password = Hash::make(Input::get('password')); // Encriptamos el password con la función Hash
		$user->level = Input::get('level');
		//$user->active = true;

		// ejecutamos la validación de los datos de usuario del formulariio
		$validator = User::validate(array(
			'username' => Input::get('username'),
			'email' => Input::get('email'),
			'password' => Input::get('password'),
			'level' => Input::get('level')
		));
		if($validator->fails()){
			$errors = $validator->messages()->all();
			$user->password = null;
			return View::make('users.formuser')->with('action','new')->with('user', $user)->with('errors', $errors);
		}
		else{
			$user->save();
			return Redirect::to('users')->with('status','ok_create');
		}
	}

	/*
	** Función:
	** Descripciçon: Retorna la vista de alta de usuario (action = new)
	** Parametrós:
	** Retorno: Retorna la vista de edicion de usuario (action = update)
	*/
	public function getEdit($user_id){
		$user = User::find($user_id);
		return View::make('users.formuser', array('user' => $user,'action'=>'update'));
	}

	/*
	** Función:
	** Descripciçon:
	** Parametrós:
	** Retorno
	*/

	public function postUpdate(){
		
		// Cargamos el usuario editado
		$user_id = Input::get('user_id'); // Carga el parámetro pasado por formulario
		$user = User::find($user_id); // Busca el usuario
		// Cargamos los datos del formulario en el objeto usuario
		$user->username = Input::get('username');
		$user->email = Input::get('email');
		$user->level = Input::get('level');
		//$user->active = true;

		$validator = User::validate(array(
			'username' => Input::get('username'),
			'email' => Input::get('email'),
			'password' => $user->password,
			'level' => Input::get('level')
		),$user->id);

		if($validator->fails()){

			$errors = $validator->messages()->all();
			return View::make('users.formuser')->with('action','update')->with('user', $user)->with('errors', $errors);
		}
		else{
			// Guarda los cambios y retorna a la lista de usuarios confirmando la actualización
			$user->save();
			return Redirect::to('users')->with('status','ok_update');	
		}
		

	}


	/*
	** Función: 
	** Descripciçon:
	** Parametrós:
	** Retorno
	*/
	public function getDelete($user_id){
		$user = User::find($user_id); // Localiza el usuario pasado por parámetro
		// Borra al usuario y retorna a la lista de usuarios confirmando la eliminación
		$user->delete();
		return Redirect::to('users')->with('status','ok_delete');
	}
	/*
	** Función:
	** Descripciçon:
	** Parametrós:
	** Retorno
	*/
}
?>