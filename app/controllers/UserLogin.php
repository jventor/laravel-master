<?php

class UserLogin extends BaseController{
	public function user(){
		//get POST data
		$userdata = array(
						'username' => Input::get('username'),
						'password' => Input::get('password')
		);

		if (Auth::attempt($userdata)){
			return Redirect::to('admin');
		}
		else{
			//return Redirect::to('/')->with('login_erros',true);
			return Redirect::to('login')->with('login_errors',true);
		}
	}

	public function close(){
		Auth::logout();
    	return View::make('logout');
	}

	public function open(){
		return View::make('login');
	}
}